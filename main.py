import argparse

import matplotlib.pyplot as plt
import matplotlib.animation as animation

from library.universe import Universe


def update_plot(i, body_data, ax):
    bdi = body_data[i]
    ax.clear()
    # colors = ["blue", "red", "green"]
    ax.set(xlim=[-10, 10], ylim=[-10, 10])
    ax.set_aspect('equal')
    # print(bdi)
    # xs = [b[0] for b in bdi]
    # ys = [b[1] for b in bdi]
    # ax.scatter(xs, ys, c=colors[: len(bdi)])
    # ax
    for b in bdi:
        c = plt.Circle((b[0], b[1]), 0.5)
        ax.add_patch(c)


def main():
    parser = argparse.ArgumentParser(description="3 Body problem in 2 or 3d hopefully")
    parser.add_argument(
        "integers", metavar="", type=int, nargs="+", help="number of masses"
    )
    time_step = 0.01
    time = 10
    num_it = int(time / time_step)
    universe = Universe(time_step=time_step)
    # return
    colors = ["tab:blue", "tab:red", "tab:green", "tab:purple"]
    colors = ["blue", "red", "green"]
    body_data = []
    for i in range(num_it):
        # print(i)
        universe.step()
        step_body_data = universe.get_body_data()
        # ax.set(xlim=[-10, 10], ylim=[-10, 10])
        # xs = [pos[0] for pos in step_body_data]
        # ys = [pos[1] for pos in step_body_data]
        # container = ax.plot(xs, ys, 'o')
        # artists.append(container)
        body_data.append(step_body_data)

    bx, by = [[0, 0, 0], [0, 0, 0]]
    # fig = plt.figure()
    # scat = plt.scatter(bx, by, c=colors, s=100)
    # ani = animation.ArtistAnimation(fig=fig, artists=artists, interval=30)
    fig, ax = plt.subplots(figsize=(5, 5))
    anim = animation.FuncAnimation(
        fig,
        update_plot,
        frames=len(body_data),
        interval=30,
        fargs=(body_data, ax),
        repeat=False,
    )

    writer = animation.FFMpegWriter(fps=60)
    anim.save("debug.mp4", writer=writer)
    plt.show()


if __name__ == "__main__":
    main()
