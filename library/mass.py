import numpy as np


class Mass(object):
    def __init__(self, mass, x, y, z=0, xdot=0, ydot=0, zdot=0):
        self.mass = mass
        self.velocity = np.array([xdot, ydot, zdot])
        self.pos = np.array([x, y, z])
        self.combined = False

    def __str__(self):
        x = self.pos[0]
        y = self.pos[1]
        z = self.pos[2]
        xdot = self.velocity[0]
        ydot = self.velocity[1]
        zdot = self.velocity[2]
        return f"mass: {self.mass}, pos: ({x}, {y}, {z}), vel: ({xdot}, {ydot}, {zdot})"

    def get_velocity(self):
        return self.velocity

    def get_pos(self):
        return self.pos

    def update_pos(self, pos_delta):
        self.pos = self.pos + pos_delta

    def update_vel(self, vel):
        self.velocity = self.velocity + vel

    def update_velocity(self, G, b2):
        c = G * b2.mass * self.mass
        rvec = b2.get_pos() - self.pos
        rmag = np.linalg.norm(rvec)
        p = c * rvec / (rmag**3)
        vel = p / self.mass
        self.update_vel(vel)

    def mark_combined(self):
        self.combined = True

    def combine(self, b2):
        b2.mark_combined()
        m1 = self.mass
        m2 = b2.mass
        v1 = self.velocity
        v2 = b2.velocity
        p1 = self.pos
        p2 = b2.pos
        new_mass = m1 + m2
        new_vel = (m1 * v1 + m2 * v2) / new_mass
        new_pos = (m1 * p1 + m2 * p2) / new_mass
        self.mass = new_mass
        self.velocity = new_vel
        self.pos = new_pos

    def is_colliding(self, b2):
        if b2.combined:
            return False
        b1_pos = self.pos
        b2_pos = b2.pos
        dist = np.linalg.norm(b1_pos - b2_pos)
        if dist < 1:
            return True


