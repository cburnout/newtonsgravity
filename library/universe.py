import random

import matplotlib.pyplot as plt
import numpy as np

from .mass import Mass

DEBUG = True
# DEBUG = False
DEBUG_BODY_1 = Mass(10, 0, 0, xdot=0.5)
DEBUG_BODY_2 = Mass(1, 5, 0, xdot=2, ydot=-3)
DEBUG_BODY_3 = Mass(1, -5, 0, ydot=5)
DEBUG_BODIES = [DEBUG_BODY_1, DEBUG_BODY_2, DEBUG_BODY_3]


class Universe(object):
    def __init__(self, num_bodies=3, G=0.1, time_step=0.01):
        self.time_step = time_step
        self.G = G
        self.bodies = [
            Mass(
                random.randint(1, 10),
                random.randint(-10, 10),
                random.randint(-10, 10),
                xdot=random.random(),
                ydot=random.random(),
            )
            for i in range(num_bodies)
        ]
        if DEBUG:
            self.bodies = DEBUG_BODIES

        self.create_body_pairs()

    def create_body_pairs(self):
        bodies_set = []
        for i, b1 in enumerate(self.bodies):
            for j, b2 in enumerate(self.bodies[i + 1 :]):
                bodies_set.append((b1, b2))
        self.bodies_set = bodies_set

    def combine_bodies(self):
        list_of_bodies_to_remove = []
        for i, b1 in enumerate(self.bodies):
            for j, b2 in enumerate(self.bodies[i + 1 :]):
                if b1.is_colliding(b2):
                    b1.combine(b2)
                    list_of_bodies_to_remove.append(b2)
        for b in list_of_bodies_to_remove:
            self.bodies.remove(b)
        self.create_body_pairs()

    def update_velocities(self):
        for b1, b2 in self.bodies_set:
            b1.update_velocity(self.G, b2)
            b2.update_velocity(self.G, b1)

    def step(self):
        ts = self.time_step
        self.combine_bodies()
        self.update_velocities()
        for b in self.bodies:
            pos_delta = b.get_velocity() * ts
            b.update_pos(pos_delta)

    def plot_bodies(self):
        for b in self.bodies:
            bpos = b.get_pos()
            plt.plot(bpos[0], bpos[1], "o", markersize=10)
        plt.show()

    def get_body_data(self):
        return [b.get_pos() for b in self.bodies]
